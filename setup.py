from setuptools import setup
from setuptools.extension import Extension
from pybind11 import get_include


# external module
ext_modules = [
    Extension(
        name="callback",
        sources=["src/pybind11/callback.cpp", "src/c/callback.cpp"],
        include_dirs=[get_include(), "src/c"],
        language="c++",
        extra_compile_args=['-O3', '-fpic', '-std=c++11']
    )
]

# installation
setup(
    name='testcallback',
    description='A Python package that tests callbacks with pybind11.',
    author='Giuseppe Congedo',
    email='giuseppe.congedo@ed.ac.uk',
    ext_modules=ext_modules
)
