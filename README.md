# Test Python callbacks from C++ with pybind11

Author
======

**Giuseppe Congedo**

Institute for Astronomy <br>
Royal Observatory of Edinburgh <br>
University of Edinburgh <br>
Blackford Hill <br>
Edinburgh <br>
EH9 3HJ <br>
UK

**<giuseppe.congedo@ed.ac.uk>**

Copyright © 2019 Giuseppe Congedo

Description
======

Example module to test how Python callbacks from C++ can be implemented with [pybind11](https://pybind11.readthedocs.io/).
Arbitrary ndarray-like Python objects or lists are cast to `std::vector<double>` and passed to the Python function to be called from within C++ at runtime.
The Python function is cast to `std::function<double(std::vector<double>)>`.

Files:
- Source: `src/c/callback.cpp`;
- Wrapper: `src/pybind11/callback.cpp`.

Instructions
======

To build the module with setuptools:
```
python setup.py build_ext --inplace
```
Or manually:
```
c++ -O3 -Wall -shared -std=c++11 -fPIC `python -m pybind11 --includes` -I src/c src/c/callback.cpp src/pybind11/callback.cpp -o callback`python-config --extension-suffix`
```
To test the running:
```
python test_callback.py
```
