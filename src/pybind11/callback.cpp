#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>
#include "callback.hpp"


PYBIND11_MODULE(callback, mod) {
    mod.def("test", &test);
}