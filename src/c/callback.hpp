#include <functional>
#include <vector>


double test(std::vector<double> x, const std::function<double(std::vector<double>)> &fun);
