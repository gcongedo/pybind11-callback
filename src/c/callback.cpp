#include <cstdio>
#include "callback.hpp"



double test(std::vector<double> x, const std::function<double(std::vector<double>)> &fun)
{

    printf("Calling the Python function from C++\n");

    double f = fun(x);

    printf("The value of the function is %f\n", f);

    printf("Returning the result\n");

    return (f);
}
