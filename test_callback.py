import numpy as np
from callback import test


def norm(x):
    if np.isscalar(x):
        return x
    else:
        return np.sqrt((np.asarray(x) ** 2).sum())


print('Testing callback with an input tuple')

x = [1., 2.]
test(x, norm)

print('Testing callback with an input array')

x = np.array([1., 2.])
test(x, norm)
